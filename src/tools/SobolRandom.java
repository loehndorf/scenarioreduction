package tools;

import java.util.Random;

import org.apache.commons.math3.distribution.NormalDistribution;
import org.apache.commons.math3.random.SobolSequenceGenerator;


public class SobolRandom extends Random {

	private static final long serialVersionUID = -8199948546274247496L;
	SobolSequenceGenerator _sobol;
	int _dim;
	int _k;
	double[] _next;
	NormalDistribution _dist;
	
	public SobolRandom (int dim) {
		_sobol = new SobolSequenceGenerator(dim);
		_dist = new NormalDistribution();
		_sobol.skipTo(1); //zero causes trouble with inversion
		_dim = dim;
		_k = dim-1;
	}
	
	@Override
	public double nextDouble() {
		if (++_k==_dim) {
			_next = _sobol.nextVector();
			_k=0;
		}
		return _next[_k];
	}
	
	@Override 
	public double nextGaussian() {
		return _dist.inverseCumulativeProbability(nextDouble());
	}
	
}
