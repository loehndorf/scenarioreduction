package tools;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.LinkedList;
import java.util.Map;

import tools.kdtree.KdTree;
import tools.kdtree.SquaredEuclidean;


/**
 * Wrapper class for a metric tree to facilitate nearest neighbor queries over a fixed set of points. 
 * @author Nils Loehndorf
 *
 * @param <E>
 */
public class MetricTree<E> implements Serializable {

	private static final long serialVersionUID = 5100441495706307337L;

	KdTree<E> kdtree;
	SquaredEuclidean dist;
	Map<double[],E> map = new LinkedHashMap<>();
	int _dimension;
	double[] _weights;
	List<double[]> removedPoints;
	public static long globaltime = 0;
	
	public MetricTree(int dimension) {
		_dimension = dimension;
		_weights = new double[dimension];
		Arrays.fill(_weights,1);
		kdtree = new KdTree<E>(dimension);
		dist = new SquaredEuclidean();
		removedPoints = new LinkedList<>();
	}
	
	/**
	 * Add the given element to the tree at the given location.
	 * @param point
	 * @param e
	 */
	public synchronized void add(double[] point, E e) {
		if (!removedPoints.isEmpty())
			rebuildTree();
		map.put(point,e);
		kdtree.addPoint(point, e);
//		try {
//			kdtree.insert(normalize(point), e);
//		} catch (KeySizeException | KeyDuplicateException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
	}
	
	/**
	 * Retrieve the element closest to the given location from the tree.
	 * @param point
	 * @return Return the nearest element.
	 */
	public synchronized E get(double[] point) {
		if (!removedPoints.isEmpty())
			rebuildTree();
		return kdtree.findNearestNeighbors(point, 1, dist).getMax();

	}
	

	
	/**
	 * Checks whether an element can be found that has the given point as its key. Compares by references not by value.
	 * @param point
	 * @return
	 */
	public boolean contains(double[] point) {
		if (!removedPoints.isEmpty())
			rebuildTree();
		if (map.containsKey(point))
			return true;
		return false;
	}
	
	/**
	 * Retrieves all elements that are stored with this tree.
	 * @return
	 */
	public Collection<E> getAll() {
		if (!removedPoints.isEmpty())
			rebuildTree();
		return map.values();
	}
	
	/**
	 * Retrieves all elements that are stored with this as a list.
	 * @return
	 */
	public List<E> getList() {
		if (!removedPoints.isEmpty())
			rebuildTree();
		List<E> l = new LinkedList<>();
		l.addAll(map.values());
		return l;
	}
	
	/**
	 * Retrieves all elements that are stored with this as a list.
	 * @return
	 */
	public Map<double[],E> getMapping() {
		return map;
	}

	/**
	 * @return Removes and returns the element associated with the given point from the tree. Compares by references not by value.
	 */
	public synchronized E remove(double[] point) {
		removedPoints.add(point);
		return map.remove(point);
	}
	
	void rebuildTree() {
		kdtree = new KdTree<E>(_dimension);
		for (double[] p : map.keySet())
			add(p,map.get(p));
		removedPoints.clear();
	}
	
	/**
	 * @return Returns the size of the tree.
	 */
	public int size() {
		if (!removedPoints.isEmpty())
			rebuildTree();
		return map.size();
	}
	

}
