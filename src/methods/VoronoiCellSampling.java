package methods;

import java.util.LinkedHashMap;
import java.util.Map;
import distribution.MultivariateDistribution;
import tools.MetricTree;

public class VoronoiCellSampling extends OptimalQuantization {
	

	public VoronoiCellSampling(MultivariateDistribution dist, long seed) {
		super(dist,seed);

	}
	
	@Override
	public Map<double[],Double> getScenarios(int numScen) {
		Map<double[],Double> mathfiMap = super.getScenarios(numScen);
		MetricTree<double[]> tree = new MetricTree<>(super._mvdist.getDim());
		for (double[] x : mathfiMap.keySet())
			tree.add(x, x);
		//randomly sample from voronoi cells
		Map<double[],double[]> builderMap = new LinkedHashMap<>();
		while (mathfiMap.size()>builderMap.size()) {
			double[] x = super._mvdist.getRealization();
			double[] nearest = tree.get(x);
			if (!builderMap.containsKey(nearest)) 
				builderMap.put(nearest,x);
		}
		Map<double[],Double> resultMap = new LinkedHashMap<>();
		for (double[] nearest : builderMap.keySet())
			resultMap.put(builderMap.get(nearest),mathfiMap.get(nearest));
		return resultMap;
	}
	

}
