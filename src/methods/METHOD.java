package methods;

public enum METHOD {
	MonteCarlo, QuasiMonteCarlo, MomentMatching, OptimalQuantization, VoronoiCellSampling
}
