package methods;


import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import distribution.MultivariateDistribution;
import distribution.MultivariateLognormal;
import distribution.MultivariateNormal;
import distribution.MultivariateStudent;
import tools.MetricTree;
import tools.Metrics;
import tools.SobolRandom;
import tools.Xorshift;


public class OptimalQuantization extends ReductionMethod {
	
	MultivariateDistribution _mvdist;
	long _seed;
//	double _minDev;
//	double _maxDev;
	
	
	public OptimalQuantization (MultivariateDistribution dist, long seed) {
		_mvdist = dist;
		_seed = seed;
	}
	
//	public static void main (String... args) {
//		int dim = 10;
//		int numScen = 50;
//		double[] mean = new double[dim];
//		double[] df = new double[dim];
//		double[] scale = new double[dim];
//		double[][] correl = new double[dim][dim];
//		for (int i=0; i<dim; i++) {
//			mean[i] = 0;
//			scale[i] = 2+i*i*i;
//			df[i] = 5;
//			correl[i][i] = 1;
//			for (int j=0; j<dim; j++)
//				if (i!=j) correl[i][j] = 0.;
//		}
//		MultivariateDistribution mvdist = new MultivariateNormal(mean,correl,new Random());
////		MultivariateDistribution mvdist = new MultivariateStudent(mean,scale,correl,df,new Random());
//		OptimalQuantization pb = new OptimalQuantization(mvdist,mvdist.getGenerator().nextLong());
//		Map<double[],Double> map = pb.getScenarios(numScen);
//		for (double[] x : map.keySet()) {
//			System.out.print(map.get(x)+"\t");
//			for (int i=0; i<x.length; i++)
//				System.out.print(x[i]+"\t");
//			System.out.println();
//		}
//	}
	
	@Override
	public Map<double[], Double> getScenarios(int numScen) {
		int dim = _mvdist.getDim();
		double a = 100*numScen;
		double sampleSize = 10000*numScen;
		final List<double[]> scenarios = new LinkedList<>();
		_mvdist.getGenerator().setSeed(_seed);
		MonteCarlo rand = new MonteCarlo(_mvdist,new Xorshift());
		for (double[] d : rand.getScenarios(numScen).keySet()) 
			scenarios.add(d);
		for (int i=1; i<sampleSize; i++) {
			double[] point = _mvdist.getRealization();
			double[] nearest = null;
			double minDist = Double.MAX_VALUE;
			for (double[] d : scenarios) {
				double dist = Metrics.squaredDistance(d, point);
				if (dist<minDist) {
					minDist = dist;
					nearest = d;
				}
			}
			for (int d=0; d<dim; d++)
				nearest[d] += a/(a+i)*(point[d]-nearest[d]);
    	}
		MetricTree<Partition> tree = new MetricTree<>(dim);
		for (double[] d : scenarios) 
			tree.add(d,new Partition(d));
		_mvdist.getGenerator().setSeed(_seed);
		for (int i=0; i<sampleSize; i++) {
			double[] x = _mvdist.getRealization();
			Partition p = tree.get(x);
			p.count++;
//			p.distance += Metrics.squaredDistance(x,p.center);
		}
//		_minDev = Double.POSITIVE_INFINITY;
//		_maxDev = 0;
//		double globaldist = 0;
		Map<double[],Double> map = new HashMap<>();
		for (double[] d : tree.getMapping().keySet()) {
			Partition p =tree.get(d);
			map.put(d,p.count*1./sampleSize);
//			double dev = p.count*1./sampleSize*Math.sqrt(p.distance/p.count);
//			if (dev < _minDev)
//				_minDev = dev;
//			if (dev > _maxDev)
//				_maxDev = dev;
//			System.out.println(p.count/sampleSize+"\t"+p.distance/p.count+"\t"+p.count*1./sampleSize*Math.sqrt(p.distance/p.count)+"\t"+Metrics.Euclidean(p.center, _mvdist.getMean())+"\t");
//			globaldist += p.distance;
		}
//		System.out.print(getPartitionVariabilityRatio()+"\t"+globaldist/sampleSize);
		return map;
	}
	
//	public double getPartitionVariabilityRatio() {
//		return _maxDev/_minDev;
//	}
	
	class Partition {
		int count;
//		double distance;
		double[] center;
		
		Partition(double[] x) {
			center = x;
		}
	}
	
	

	
}
