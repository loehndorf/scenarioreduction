package methods;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Random;

import distribution.MultivariateDistribution;

public class MonteCarlo extends ReductionMethod {
	
	MultivariateDistribution _mvDist;
	Random _generator;
	
	public MonteCarlo (MultivariateDistribution mvDist, Random generator) {
		_mvDist = mvDist;
		_generator = generator;
		
	}
	
	@Override
	public Map<double[], Double> getScenarios(int numScen) {
		Random oldGen = _mvDist.getGenerator();
		_mvDist.setGenerator(_generator);
		Map<double[],Double> resultMap = new LinkedHashMap<>();
		for (int i=0; i<numScen; i++)
			resultMap.put(_mvDist.getRealizationByInversion(),1./numScen);
		_mvDist.setGenerator(oldGen);
		return resultMap;
	}
}
