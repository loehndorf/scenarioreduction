package application;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Arrays;
import java.util.Map;

import tools.Xorshift;
import distribution.MultivariateDistribution;
import distribution.MultivariateLognormal;
import distribution.MultivariateNormal;
import distribution.MultivariateStudent;
import distribution.MultivariateUniform;
import distribution.DISTRIBUTION;
import measures.CovarianceDiscrepancy;
import measures.NewsvendorMetric;
import measures.WassersteinDistance;
import methods.METHOD;

/**
 * <p>The Experiment class executes a number of scenario reduction methods for over a set of probability distributions for benchmarking purposes.</p>
 * @author Nils Loehndorf
 *
 */
public class Experiment {
	
	static int _baseDemand = 1;
	static int _sampleSize = 100000;
	static long _seed = 191;
	static double _df = 5; //only Student distribution
	static String filename = "experiment_"+System.nanoTime()+".txt";
	static double[] correlation = new double[]{0.0,0.5};
	static double[] coefficientOfVariation = new double[]{0.33,1.0};
	static int[] dimensions = new int[]{1,10,25};
	static int[] numScens = new int[]{5,50,500};
	static DISTRIBUTION[] dists = new DISTRIBUTION[]{DISTRIBUTION.Normal,DISTRIBUTION.Uniform,DISTRIBUTION.Lognormal,DISTRIBUTION.Student};
	static METHOD[] methods = new METHOD[]{METHOD.VoronoiCellSampling,METHOD.MomentMatching,METHOD.QuasiMonteCarlo,METHOD.OptimalQuantization};
	
	public static void main (String... args) throws IOException {	
		FileOutputStream stream = new FileOutputStream(filename);
		BufferedWriter br = new BufferedWriter(new OutputStreamWriter(stream));
		String s = "Distribution\tDimensions\tScenarios\tCV\trho\tMethod\tNewsvendorExpected\tNewsvendorImplemented\tCovarianceDiscrepancy\tWassersteinBound\tWassersteinDistance\n";
		br.write(s);br.flush();
		System.out.print(s);
		for (DISTRIBUTION dist : dists) {
			for (int dim : dimensions) {
				for (int numScen : numScens) {
					for (double cv : coefficientOfVariation) {
						for (double cor : correlation) {
							MultivariateDistribution mvDist = null;
							double[] means = new double[dim];
							double[][] cov = new double[dim][dim];
							for (int i=0; i<dim; i++) {
								means[i] = _baseDemand;
								cov[i][i] = _baseDemand*cv*_baseDemand*cv;
							}
							for (int i=0; i<dim; i++) {
								for (int j=i+1; j<dim; j++) {
									cov[i][j] = Math.sqrt(cov[i][i]*cov[j][j])*cor;
									cov[j][i] = cov[i][j];
								}
							}
							switch(dist) {
							case Normal : 
								mvDist = new MultivariateNormal(means,cov,new Xorshift(_seed));	
								break;
							case Lognormal : 
								mvDist = new MultivariateLognormal(means,cov,new Xorshift(_seed));	
								break;
							case Uniform : 
								mvDist = new MultivariateUniform(means,cov,new Xorshift(_seed));	
								break;
							case Student : 
								double[] df = new double[dim];
								double[] scale = new double[dim];
								double[][] correl = new double[dim][dim];
								for (int i=0; i<dim; i++) {
									scale[i] = Math.sqrt(cov[i][i]);
									for (int j=0; j<dim; j++)
										if (i!=j)
											correl[i][j] = cor;
										else
											correl[i][j] = 1;
								}
								
								Arrays.fill(df,_df);
								mvDist = new MultivariateStudent(means,scale,correl,df,new Xorshift(_seed));	
								break;
							}
							for (METHOD method : methods) {
								double[] score = null;
								double wasser1 = 0.;
								double wasser2 = 0.;
								double correl = 0.;
								Map<double[],Double> scen = null;
								try {
									scen = Generator.runScenarioGenerator(numScen,method,mvDist,_sampleSize,_seed);
								}
								catch (Exception e) {
									e.printStackTrace();
								};
								if (scen==null)
									scen = Generator.runScenarioGenerator(numScen,METHOD.MonteCarlo,mvDist,_sampleSize,_seed);
								score = NewsvendorMetric.getNewsvendorMetric(scen,mvDist,_sampleSize,_seed);
								correl = CovarianceDiscrepancy.getDiscrepancy(scen, mvDist);
								wasser1 = WassersteinDistance.getLowerBound(scen,mvDist,_sampleSize,_seed);
								wasser2 = WassersteinDistance.getExactDistance(scen,mvDist,_sampleSize,_seed);
								s = dist.toString()+"\t"+dim+"\t"+numScen+"\t"+cv+"\t"+cor+"\t"+method.toString()+"\t"+score[0]+"\t"+score[1]+"\t"+correl+"\t"+wasser1+"\t"+wasser2+"\n";
								br.write(s);br.flush();
								System.out.print(s);
							}
						}
					}
				}
			}
		}
		br.close();
	}

}
