
$if not set solvername $setglobal solvername sulum

$eolcom //
set g 'sulum Option Groups' /
           Simplex "Options related to the simplex optimizer"
               MIP "Options related to the mixed integer optimizer"
           Logging "Options related to logging in Sulum"
          Presolve "Options related to the presolve module"
             Other "Other options"
    /
    e / '-1',0*100 /,
    f / def Default, lo Lower Bound, up Upper Bound, ref Reference /,
    t / I   Integer
        R   Real
        S   String
        B   Binary
    /
$onempty
    m(*) / reslim /  // eventually we we get them as %system.gamsopt% or so
$offempty

    o options /
     optimizer                       "Controls which optimizer will be used."
     presolve                        "Controls which type of presolve strategy should be used by the presolve module."
     presolvehotstart                "Controls which type of presolve strategy should be used by the presolve module, when a hotstart is present."
     presolvecompress                "Controls if the problem should be compressed after a call to the presolve module."
     presolvecompresshotstart        "Controls if the problem should be compressed after a call to the presolve module were a hotstart is present."
     simprimprice                    "Controls which pricing strategy should be used by the primal simplex optimizer."
     simdualprice                    "Controls which pricing strategy should be used by the dual simplex optimizer."
     simprimpricehotstart            "Controls which pricing strategy should be used by the primal simplex optimizer, when a hotstart is available."
     simdualpricehotstart            "Controls which pricing strategy should be used by the dual simplex optimizer, when a hotstart is available."
     loglevel                        "Controls the amount of output from Sulum in general."
     simloglevel                     "Controls the amounts of output from the simplex optimizer."
     simquadprecision                "Controls if quad precision is used in the simplex optimizer."
     simperturblevel                 "Controls the level of perturbations in the simplex optimizer."
     simshifting                     "Controls the if shifting is used in the simplex optimizer."
     simwarmstart                    "If this key is switched off then the optimizer disregard any solution stored in the model."
     simprob                         "Some time it might be faster to solve the respective dual formulation instead of the primal."
     primcrash                       "Controls if the primal simplex optimizer should crash a advanced start basis."
     dualcrash                       "Controls if the dual simplex optimizer should crash a advanced start basis."
     simscale                        "Controls if the simplex optimizer should scale data to be more numerical stable."
     simscalehotstart                "Controls if the simplex optimizer should scale data to be more numerical stable, when a hotstart is present."
     objsense                        "Decide if the model should be maximized or minimized."
     unused                          "Unused enum will be used later."
     mpswritenames                   "Controls if Sulum MPS writer should replace constraint and variable names with generic ones ."
     debug                           "This option can be switch on in debug mode and development phase to find bugs easier."
     logprefix                       "Skip doing prefix in logging i.e stream tags like info,debug,log etc is stripped."
     lognomodulemessage              "Skip writing stop and start for each optimizer module in logging."
     simmaxiter                      "Maximum iterations allowed in simplex optimizers."
     updatesolquality                "Decides if the optimizer should update information solution quality items at the end of a call to the optimizer."
     simobjcutnosol                  "Controls if a solution is needed when the optimizer stops premature due to objective cut ."
     lpwritenames                    "Controls if Sulum LP writer should replace constraint and variable names with generic ones ."
     lpwritenumonline                "Controls how many items Sulum LP writer should write on each line."
     lpwriteprecision                "Controls how many items Sulum LP writer should write on each line."
     lpwritefreecons                 "Controls if Sulum LP writer should write free constraints."
     mpswritefreecons                "Controls if Sulum MPS writer should write free constraints."
     lpreadfreecons                  "Controls if Sulum LP reader should read free constraints."
     mpsreadfreecons                 "Controls if Sulum MPS reader should read free constraints."
     lpwritevarorder                 "Controls if Sulum LP writer should write variables in same order i.e write variables with zero objective in objective section."
     numthreads                      "Controls the maximum number of threads used by the Sulum optimizer, if set to zero Sulum will use a number of threads corresponding to number of cores detected."
     mipmaxnodes                     "Controls the maximum number of nodes processed bu the Sulum MIP optimizer."
     mipmaxsol                       "Controls the maximum number of feasible solutions found and stored in the solution pool by the Sulum MIP optimizer."
     mipcuts                         "Controls the overall level of cut generation used by the Sulum MIP optimizer."
     mipmircuts                      "Controls the level of cut generation in MIR cuts used by the Sulum MIP optimizer."
     mipgomorycuts                   "Controls the level of cut generation in Gomory cuts used by the Sulum MIP optimizer."
     mipflowcovercuts                "Controls the level of cut generation in flow cover cuts used by the Sulum MIP optimizer (Not implemented yet)."
     mipcliquecuts                   "Controls the level of cut generation in clique cuts used by the Sulum MIP optimizer."
     mipimpliedboundcuts             "Controls the level of cut generation in implied bound cuts used by the Sulum MIP optimizer."
     mipzerohalfcuts                 "Controls the level of cut generation in zero half cuts used by the Sulum MIP optimizer (Not implemented yet)."
     mipnodeselect                   "Controls how nodes are selected in Sulum MIP optimizer."
     mipbranchselect                 "Controls how branching variables are selected in Sulum MIP optimizer."
     mipnodechildselect              "Controls how node selection on child nodes are selected in Sulum MIP optimizer."
     mipsolveaslp                    "Controls Sulum Optimizer should solve a MIP as LP i.e relaxing integer constraints."
     miplocalnodepresolve            "Controls Sulum MIP Optimizer should use node presolve at local nodes i.e nodes besides the root node."
     miplocalnodeheuristic           "Controls Sulum MIP Optimizer should use node heuristics at local nodes i.e nodes besides the root node."
     miplocalnodeheuristicpump       "Controls Sulum MIP Optimizer should use node pumping heuristics at local nodes i.e nodes besides the root node."
     miplocalnodecuts                "Controls Sulum MIP Optimizer should use node cuts at local nodes i.e nodes besides the root node."
     mipreducedcoststrengthroot      "Controls if the Sulum MIP Optimizer should use reduced cost strengthening at the root node."
     mipreducedcoststrengthnode      "Controls if the Sulum MIP Optimizer should use reduced cost strengthening at the each node."
     simusequadinf                   "Controls if Sulum Simplex Optimizer should use switch to quad precision, when a problem is determined infeasible."
     miploglevel                     "Controls the amount of output from Sulum integer optimizer in general."
     mipnodeselectloglevel           "Controls the amount of output from Sulum integer optimizer in the node selection module."
     mipbranchselectloglevel         "Controls the amount of output from Sulum integer optimizer in the branch selection module."
     mipintsolloglevel               "Controls the amount of output from Sulum integer optimizer about finding integer feasible solutions."
     simsolveunscaled                "Controls if the simplex optimizer should reoptimize on a unscaled problem if tolerances are not met."
     mipfeasfocus                    "Controls if the mixed integer optimizer should focus more on obtaining a feasible solution."
     mipsubmippinglevel              "Controls how deep the mixed integer optimizer can go on solve sub mips."
     mipsolstopnum                   "Controls if the mixed integer optimizer should stop after a certain number of integer solutions is found."
     mipdotiming                     "Controls if the mixed integer optimizer should do extra timing which can be fetched from varies information items."
     mipstalllimit                   "Controls if the mixed integer optimizer should be stopped prematurely due to no progress."
     mippresolverootprobing          "Controls if the mixed integer optimizer should apply probing techniques in presolve."
     miplocalnodeprobing             "Controls if the mixed integer optimizer can apply probing techniques in a local node."
     miprootheuristicrens            "Controls Sulum MIP Optimizer should use node RENS(Relaxation Enforced Neighborhood Search) rounding heuristics in the root node."
     miplocalnodeheuristicrens       "Controls Sulum MIP Optimizer should use node RENS(Relaxation Enforced Neighborhood Search) rounding heuristics at local nodes."
     miprootheuristicrins            "Controls Sulum MIP Optimizer should use node RENS(Relaxation Enforced Neighborhood Search) rounding heuristics in the root node."
     miplocalnodeheuristicrins       "Controls Sulum MIP Optimizer should use node RINS(Relaxation Induced Neighborhood Search) improvement heuristics at local nodes."
     miprootheuristicsins            "Controls Sulum MIP Optimizer should use node SINS(Sulum Induced Neighborhood Search) improvement heuristics in the root node."
     miplocalnodeheuristicsins       "Controls Sulum MIP Optimizer should use node SINS(Sulum Induced Neighborhood Search) improvement heuristics at local nodes."
     miprootheuristicdiving          "Controls Sulum MIP Optimizer should use node diving heuristics in the root node."
     miplocalnodeheuristicdiving     "Controls Sulum MIP Optimizer should use node diving heuristics at local nodes."
     mipadvancedpseudocost           "Controls if Sulum MIP Optimizer should use advanced techniqes in maintaining pseudo costs."
     mipmaxsimiter                   "Controls the maximum number of simlex iterations processed by the Sulum MIP optimizer."
     randomseed                      "Controls the random seed by the Sulum MIP optimizer."
     mipstronginit                   "Controls if the mixed integer optimizer should apply strong branching initialize in non strong branching rules."
     mipgomoryfractionalcuts         "Controls the level of cut generation in Gomory fractional cuts used by the Sulum MIP optimizer."
     mipknapsackcovercuts            "Controls the level of cut generation in knapsack cover cuts used by the Sulum MIP optimizer."
     mipcutsuselexdual               "Controls if lexicographic dual simplex is used by the Sulum MIP optimizer in cut reoptimization."
     mipcutsinlpprround              "Controls the maximum number of cuts added in each reoptimizing round used by the Sulum MIP optimizer in cut reoptimization."
     miprootheuristicswop            "Controls Sulum MIP Optimizer should use swopping heuristic in the root node."
     mipnodeheuristicswop            "Controls Sulum MIP Optimizer should use swopping heuristic in the tree nodes."
     miprootheuristiceasy            "Controls Sulum MIP Optimizer should use easy heuristic in the root node."
     mipnodeheuristiceasy            "Controls Sulum MIP Optimizer should use easy heuristic in the tree nodes."
     miptwomircuts                   "Controls the level of cut generation in two MIR cuts used by the Sulum MIP optimizer."
     mipmaxrestarts                  "Controls the level of maximum number MIP restarts used by the Sulum MIP optimizer."
     mipusereformulation             "Controls if Sulum MIP optimizer is allowed to reformulate the MIP problem."
     solwritenames                   "Controls if Sulum solution writer should replace constraint and variable names with generic ones ."
     cloudmodelmethod                "Controls Sulum Optimizer should handle model instances numbering when a new cloud call is being made ."
     cloudusehttpgzip                "Controls if Sulum cloud transfers should be gzipped ."
     cloudusehttps                   "Controls if Sulum cloud transfers should be HTTPS ."
     cloudagentselect                "Controls how Sulum cloud selects agents ."
     cloudagentselectnum             "Controls how many agents Sulum cloud selects ."
     mipdeterministic                "Determines if Sulum mixed Integer Optimizer should use deterministic mode."
     mipparallelrootsolve            "Determines if Sulum mixed Integer Optimizer should use parallel solves of multiple root nodes, exchange information between solves and then picking the best one."
     mipmaxvariability               "Determines if Sulum mixed Integer Optimizer should try to generate different paths, so the user can exploit variability."
     miphotstart                     "Use previous MIP solutions when present."
     miphotstartrins                 "Use RINS to repair previous solutions when a hotstart is present."
     bndoptlevel                     "Determines the level of effort used in the bound optimizer module."
     bndoptmip                       "Determines if MIP formulations can be used in the bound optimizer module."
     bndoptmaxnz                     "Determines maximum of non zeroes which can be used in the bound optimizer module."
     bndoptmaxiter                   "Determines maximum of iterations which can be used in the bound optimizer module."
     bndoptmaxnodes                  "Determines maximum of nodes which can be used in the bound optimizer module."
     simusenetwork                   "Determines if the simplex optimizer will look for complete network structure and use the network optimizer when no hotstart is present."
     simusenetworkhotstart           "Determines if the simplex optimizer will look for complete network structure and use the network optimizer when a hotstart is present."
     simtolprim                      "Absolute tolerance used by the simplex optimizer to determine if a solution is primal feasible or not."
     simtoldual                      "Absolute tolerance used by the simplex optimizer to determine if a solution is dual feasible or not."
     simtolpivot                     "Absolute tolerance used by the simplex optimizer to control the minimum size of a pivot element."
     simtolmarko                     "Absolute tolerance used by the simplex optimizer to control the stability of pivot size in LU factorization module."
     simtimelimit                    "Maximum time allowed in the simplex optimizer."
     simobjupcut                     "If the optimal objective value can be proved to be larger than this value, then the optimizer terminates."
     simobjlocut                     "If the optimal objective value can be proved to be less than this value, then the optimizer terminates."
     wrnlargea                       "If a absolute value in the constraint matrix is larger than this value a warning will be displayed, but only if debug is on."
     wrnsmalla                       "If a absolute value in the constraint matrix is smaller than this value a warning will be displayed, but only if debug is on."
     wrnlargec                       "If a absolute value in the objective is larger than this value a warning will be displayed, but only if debug is on."
     wrnlargelo                      "If the absolute value of a lower bound is larger than this value a warning will be displayed, but only if debug is on."
     wrnlargeup                      "If the absolute value of a upper bound is larger than this value a warning will be displayed, but only if debug is on."
     opttimelimit                    "Maximum time allowed in the optimizer."
     optsolvezero                    "Tolerance on what is considered zero in solves with basis call by the user."
     miptolrelgap                    "Relative stopping tolerance used by the MIP optimizer."
     miptolabsgap                    "Absolute stopping tolerance used by the MIP optimizer."
     miptolint                       "Integer variable tolerance used by the MIP optimizer."
     miptimelimit                    "Maximum time allowed in the mixed integer optimizer."
     mipobjupcut                     "If the optimal objective value can be proved to be larger than this value and a feasible solution exists,, then the optimizer terminates."
     mipobjlocut                     "If the optimal objective value can be proved to be less than this value and a feasible solution exists, then the optimizer terminates."
     mipcutoff                       "Cutoff value to be used to prune nodes in the mixed integer optimizer."
     mipcutmaxadd                    "Maximum fraction of cuts added compared to number of constraints in the mixed integer optimizer."
     mipbigm                         "Maximum big M penalty used in MIP reformulations performed by the mixed integer optimizer."
    /

$onembedded
    optdata(g,o,t,f) /
      Simplex.(
        simprimprice                    .i.(def 0, lo 0, up 3, ref 5)
        simdualprice                    .i.(def 0, lo 0, up 3, ref 6)
        simprimpricehotstart            .i.(def 0, lo 0, up 3, ref 7)
        simdualpricehotstart            .i.(def 0, lo 0, up 3, ref 8)
        simloglevel                     .i.(def 5, lo 0, up 10, ref 10)
        simquadprecision                .i.(def 0, lo 0, up 1, ref 11)
        simperturblevel                 .i.(def 50, lo 0, up 100, ref 12)
        simshifting                     .i.(def 1, lo 0, up 1, ref 13)
        simwarmstart                    .i.(def 1, lo 0, up 1, ref 14)
        simprob                         .i.(def 0, lo 0, up 2, ref 15)
        simscale                        .i.(def 1, lo 0, up 1, ref 18)
        simscalehotstart                .i.(def 1, lo 0, up 1, ref 19)
        simmaxiter                      .i.(def maxint, lo 0, up maxint, ref 29)
        simobjcutnosol                  .i.(def 0, lo 0, up 1, ref 31)
        simusequadinf                   .i.(def 1, lo 0, up 1, ref 60)
        simsolveunscaled                .i.(def 1, lo 0, up 1, ref 65)
        mipmaxsimiter                   .i.(def maxint, lo 0, up maxint, ref 82)
        simusenetwork                   .i.(def 1, lo 0, up 1, ref 112)
        simusenetworkhotstart           .i.(def 0, lo 0, up 1, ref 113)
        simtolprim                      .r.(def 1.0e-6, lo 1.0e-10, up 1.0e-4, ref 0)
        simtoldual                      .r.(def 1.0e-6, lo 1.0e-10, up 1.0e-4, ref 1)
        simtolpivot                     .r.(def 1.0e-9, lo 1.0e-12, up 1.0e-5, ref 2)
        simtolmarko                     .r.(def 8.0e-3, lo 1.0e-4, up 9.0e-1, ref 3)
        simtimelimit                    .r.(def maxdouble, lo 0, up maxdouble, ref 4)
        simobjupcut                     .r.(def maxdouble, lo mindouble, up maxdouble, ref 5)
        simobjlocut                     .r.(def mindouble, lo mindouble, up maxdouble, ref 6)
      )
      MIP.(
        mipmaxnodes                     .i.(def maxint, lo 0, up maxint, ref 41)
        mipmaxsol                       .i.(def 10, lo 1, up maxint, ref 42)
        mipcuts                         .i.(def 4, lo 0, up 4, ref 43)
        mipmircuts                      .i.(def 4, lo 0, up 4, ref 44)
        mipgomorycuts                   .i.(def 4, lo 0, up 4, ref 45)
        mipflowcovercuts                .i.(def 4, lo 0, up 4, ref 46)
        mipcliquecuts                   .i.(def 4, lo 0, up 4, ref 47)
        mipimpliedboundcuts             .i.(def 4, lo 0, up 4, ref 48)
        mipzerohalfcuts                 .i.(def 4, lo 0, up 4, ref 49)
        mipnodeselect                   .i.(def 0, lo 0, up 5, ref 50)
        mipbranchselect                 .i.(def 0, lo 0, up 5, ref 51)
        mipnodechildselect              .i.(def 0, lo 0, up 3, ref 52)
        mipsolveaslp                    .i.(def 0, lo 0, up 1, ref 53)
        miplocalnodepresolve            .i.(def 10, lo 0, up 10, ref 54)
        miplocalnodeheuristic           .i.(def 1, lo 0, up 3, ref 55)
        miplocalnodeheuristicpump       .i.(def 1, lo 0, up 3, ref 56)
        miplocalnodecuts                .i.(def 10, lo 0, up 10, ref 57)
        mipreducedcoststrengthroot      .i.(def 1, lo 0, up 1, ref 58)
        mipreducedcoststrengthnode      .i.(def 1, lo 0, up 1, ref 59)
        miploglevel                     .i.(def 5, lo 0, up 10, ref 61)
        mipnodeselectloglevel           .i.(def 0, lo 0, up 10, ref 62)
        mipbranchselectloglevel         .i.(def 0, lo 0, up 10, ref 63)
        mipintsolloglevel               .i.(def 0, lo 0, up 10, ref 64)
        mipfeasfocus                    .i.(def 0, lo 0, up 1, ref 66)
        mipsubmippinglevel              .i.(def 1, lo 0, up 10, ref 67)
        mipsolstopnum                   .i.(def maxint, lo 1, up maxint, ref 68)
        mipdotiming                     .i.(def 0, lo 0, up 1, ref 69)
        mipstalllimit                   .i.(def maxint, lo 1, up maxint, ref 70)
        mippresolverootprobing          .i.(def 1, lo 0, up 1, ref 71)
        miplocalnodeprobing             .i.(def 1, lo 0, up 1, ref 72)
        miprootheuristicrens            .i.(def 1, lo 0, up 3, ref 73)
        miplocalnodeheuristicrens       .i.(def 1, lo 0, up 3, ref 74)
        miprootheuristicrins            .i.(def 1, lo 0, up 3, ref 75)
        miplocalnodeheuristicrins       .i.(def 1, lo 0, up 3, ref 76)
        miprootheuristicsins            .i.(def 1, lo 0, up 3, ref 77)
        miplocalnodeheuristicsins       .i.(def 1, lo 0, up 3, ref 78)
        miprootheuristicdiving          .i.(def 1, lo 0, up 3, ref 79)
        miplocalnodeheuristicdiving     .i.(def 1, lo 0, up 3, ref 80)
        mipadvancedpseudocost           .i.(def 1, lo 0, up 1, ref 81)
        mipstronginit                   .i.(def 1, lo 0, up 1, ref 84)
        mipgomoryfractionalcuts         .i.(def 4, lo 0, up 4, ref 85)
        mipknapsackcovercuts            .i.(def 4, lo 0, up 4, ref 86)
        mipcutsuselexdual               .i.(def 1, lo 0, up 1, ref 87)
        mipcutsinlpprround              .i.(def 500, lo 1, up maxint, ref 88)
        miprootheuristicswop            .i.(def 1, lo 0, up 3, ref 89)
        mipnodeheuristicswop            .i.(def 1, lo 0, up 3, ref 90)
        miprootheuristiceasy            .i.(def 1, lo 0, up 3, ref 91)
        mipnodeheuristiceasy            .i.(def 1, lo 0, up 3, ref 92)
        miptwomircuts                   .i.(def 4, lo 0, up 4, ref 93)
        mipmaxrestarts                  .i.(def 5, lo 0, up 10, ref 94)
        mipusereformulation             .i.(def 1, lo 0, up 1, ref 95)
        mipdeterministic                .i.(def 1, lo 0, up 1, ref 102)
        mipparallelrootsolve            .i.(def 1, lo 0, up 1, ref 103)
        mipmaxvariability               .i.(def 0, lo 0, up 1, ref 104)
        miphotstart                     .i.(def 1, lo 0, up 1, ref 105)
        miphotstartrins                 .i.(def 1, lo 0, up 1, ref 106)
        bndoptmip                       .i.(def 1, lo 0, up 1, ref 108)
        miptolrelgap                    .r.(def 1.0e-4, lo 1.0e-16, up maxdouble, ref 15)
        miptolabsgap                    .r.(def 1.0e-10, lo 0.0, up maxdouble, ref 16)
        miptolint                       .r.(def 1.0e-6, lo 1.0e-9, up 1.0e-2, ref 17)
        miptimelimit                    .r.(def maxdouble, lo 0, up maxdouble, ref 18)
        mipobjupcut                     .r.(def maxdouble, lo mindouble, up maxdouble, ref 19)
        mipobjlocut                     .r.(def mindouble, lo mindouble, up maxdouble, ref 20)
        mipcutoff                       .r.(def maxdouble, lo mindouble, up maxdouble, ref 21)
        mipcutmaxadd                    .r.(def 0.5, lo 0.0, up 10.0, ref 22)
        mipbigm                         .r.(def 1.0e+6, lo 0.0, up 1.0e+8, ref 23)
      )
      Logging.(
        loglevel                        .i.(def 5, lo 0, up 10, ref 9)
        logprefix                       .i.(def 0, lo 0, up 1, ref 27)
        lognomodulemessage              .i.(def 0, lo 0, up 1, ref 28)
      )
      Presolve.(
        presolve                        .i.(def 1, lo 0, up 5, ref 1)
        presolvehotstart                .i.(def 1, lo 0, up 5, ref 2)
        presolvecompress                .i.(def 0, lo 0, up 2, ref 3)
        presolvecompresshotstart        .i.(def 0, lo 0, up 2, ref 4)
      )
      Other.(
        optimizer                       .i.(def 0, lo 0, up 2, ref 0)
        primcrash                       .i.(def 1, lo 0, up 1, ref 16)
        dualcrash                       .i.(def 1, lo 0, up 1, ref 17)
        objsense                        .i.(def 0, lo 0, up 1, ref 20)
        mpswritenames                   .i.(def 0, lo 0, up 1, ref 22)
        debug                           .i.(def 0, lo 0, up 1, ref 23)
        updatesolquality                .i.(def 1, lo 0, up 1, ref 30)
        lpwritenames                    .i.(def 0, lo 0, up 1, ref 32)
        lpwritenumonline                .i.(def 5, lo 1, up 20, ref 33)
        lpwriteprecision                .i.(def 4, lo 2, up 20, ref 34)
        lpwritefreecons                 .i.(def 0, lo 0, up 1, ref 35)
        mpswritefreecons                .i.(def 0, lo 0, up 1, ref 36)
        lpreadfreecons                  .i.(def 0, lo 0, up 1, ref 37)
        mpsreadfreecons                 .i.(def 0, lo 0, up 1, ref 38)
        lpwritevarorder                 .i.(def 1, lo 0, up 1, ref 39)
        numthreads                      .i.(def 1, lo 0, up maxint, ref 40)
        randomseed                      .i.(def 1234, lo 1, up maxint, ref 83)
        solwritenames                   .i.(def 0, lo 0, up 1, ref 96)
        cloudmodelmethod                .i.(def 2, lo 1, up 2, ref 97)
        cloudusehttpgzip                .i.(def 0, lo 0, up 1, ref 98)
        cloudusehttps                   .i.(def 0, lo 0, up 1, ref 99)
        cloudagentselect                .i.(def 3, lo 1, up 3, ref 100)
        cloudagentselectnum             .i.(def 1, lo 1, up 10, ref 101)
        bndoptlevel                     .i.(def 1, lo 0, up 3, ref 107)
        bndoptmaxnz                     .i.(def maxint, lo 0, up maxint, ref 109)
        bndoptmaxiter                   .i.(def maxint, lo 0, up maxint, ref 110)
        bndoptmaxnodes                  .i.(def maxint, lo 0, up maxint, ref 111)
        wrnlargea                       .r.(def 1.0e+8, lo 0.0, up maxdouble, ref 7)
        wrnsmalla                       .r.(def 1.0e+8, lo 0.0, up maxdouble, ref 8)
        wrnlargec                       .r.(def 1.0e+8, lo 0.0, up maxdouble, ref 9)
        wrnlargelo                      .r.(def 1.0e+8, lo 0.0, up maxdouble, ref 10)
        wrnlargeup                      .r.(def 1.0e+8, lo 0.0, up maxdouble, ref 11)
        opttimelimit                    .r.(def maxdouble, lo 0, up maxdouble, ref 13)
        optsolvezero                    .r.(def 1.0e-12, lo 0, up 1.0e-8, ref 14)
      )
   /
set oe(o,e) enumerated types   /
optimizer.
     (            0  "The optimizer decides which optimizer to call based on the model structure."
                  1  "The primal simplex optimizer should be applied."
                  2  "The dual simplex optimizer should be applied." )
presolve.
     (            0  "Do not apply any presolve strategies."
                  1  "The optimizer automatically decides if presolve and which type of presolve should be applied."
                  2  "Presolve will only use very simple methods to reduce problem size."
                  3  "The presolve will only use strategies based on primal information."
                  4  "The presolve will only use strategies based on dual information."
                  5  "Presolve will reduce problem size using all methods." )
presolvehotstart.
     (            0  "Do not apply any presolve strategies."
                  1  "The optimizer automatically decides if presolve and which type of presolve should be applied."
                  2  "Presolve will only use very simple methods to reduce problem size."
                  3  "The presolve will only use strategies based on primal information."
                  4  "The presolve will only use strategies based on dual information."
                  5  "Presolve will reduce problem size using all methods." )
presolvecompress.
     (            0  "The presolve automatically decides if final problem should be compressed."
                  1  "Presolve will use compression."
                  2  "Do not apply any presolve compression." )
presolvecompresshotstart.
     (            0  "The presolve automatically decides if final problem should be compressed."
                  1  "Presolve will use compression."
                  2  "Do not apply any presolve compression." )
simprimprice.
     (            0  "The simplex optimizer analyzes the model and decides the best choice in the given situation."
                  1  "The simplex optimizer will use steepest edge strategy, which is the most expensive pricing strategy, but also often the one with fewest iterations."
                  2  "The simplex optimizer will use approximate steepest edge strategy, which relaxes the steepest edge strategy by using only approximate norms."
                  3  "The simplex optimizer will scan only a subset of candidates and choose between promising candidates by a very cheap scheme." )
simdualprice.
     (            0  "The simplex optimizer analyzes the model and decides the best choice in the given situation."
                  1  "The simplex optimizer will use steepest edge strategy, which is the most expensive pricing strategy, but also often the one with fewest iterations."
                  2  "The simplex optimizer will use approximate steepest edge strategy, which relaxes the steepest edge strategy by using only approximate norms."
                  3  "The simplex optimizer will scan only a subset of candidates and choose between promising candidates by a very cheap scheme." )
simprimpricehotstart.
     (            0  "The simplex optimizer analyzes the model and decides the best choice in the given situation."
                  1  "The simplex optimizer will use steepest edge strategy, which is the most expensive pricing strategy, but also often the one with fewest iterations."
                  2  "The simplex optimizer will use approximate steepest edge strategy, which relaxes the steepest edge strategy by using only approximate norms."
                  3  "The simplex optimizer will scan only a subset of candidates and choose between promising candidates by a very cheap scheme." )
simdualpricehotstart.
     (            0  "The simplex optimizer analyzes the model and decides the best choice in the given situation."
                  1  "The simplex optimizer will use steepest edge strategy, which is the most expensive pricing strategy, but also often the one with fewest iterations."
                  2  "The simplex optimizer will use approximate steepest edge strategy, which relaxes the steepest edge strategy by using only approximate norms."
                  3  "The simplex optimizer will scan only a subset of candidates and choose between promising candidates by a very cheap scheme." )
simquadprecision.
     (            0  "The given option is off."
                  1  "The given option is on." )
simshifting.
     (            0  "The given option is off."
                  1  "The given option is on." )
simwarmstart.
     (            0  "The given option is off."
                  1  "The given option is on." )
simprob.
     (            0  "The optimizer decides if the primal or dual formulation should be solved."
                  1  "The primal formulation should be solved."
                  2  "The dual formulation should be solved." )
primcrash.
     (            0  "The given option is off."
                  1  "The given option is on." )
dualcrash.
     (            0  "The given option is off."
                  1  "The given option is on." )
simscale.
     (            0  "The given option is off."
                  1  "The given option is on." )
simscalehotstart.
     (            0  "The given option is off."
                  1  "The given option is on." )
objsense.
     (            0  "The problem is of type minimization."
                  1  "The problem is of type maximization." )
mpswritenames.
     (            0  "The given option is off."
                  1  "The given option is on." )
debug.
     (            0  "The given option is off."
                  1  "The given option is on." )
logprefix.
     (            0  "The given option is off."
                  1  "The given option is on." )
lognomodulemessage.
     (            0  "The given option is off."
                  1  "The given option is on." )
updatesolquality.
     (            0  "The given option is off."
                  1  "The given option is on." )
simobjcutnosol.
     (            0  "The given option is off."
                  1  "The given option is on." )
lpwritenames.
     (            0  "The given option is off."
                  1  "The given option is on." )
lpwritefreecons.
     (            0  "The given option is off."
                  1  "The given option is on." )
mpswritefreecons.
     (            0  "The given option is off."
                  1  "The given option is on." )
lpreadfreecons.
     (            0  "The given option is off."
                  1  "The given option is on." )
mpsreadfreecons.
     (            0  "The given option is off."
                  1  "The given option is on." )
lpwritevarorder.
     (            0  "The given option is off."
                  1  "The given option is on." )
mipcuts.
     (            0  "The given option is off i.e do no cut generation."
                  1  "Do a minimum of work in cut generation."
                  2  "Do a fair amount of work in cut generation."
                  3  "Be aggressive in cut generation."
                  4  "The given option is automatically determined by the optimizer." )
mipmircuts.
     (            0  "The given option is off i.e do no cut generation."
                  1  "Do a minimum of work in cut generation."
                  2  "Do a fair amount of work in cut generation."
                  3  "Be aggressive in cut generation."
                  4  "The given option is automatically determined by the optimizer." )
mipgomorycuts.
     (            0  "The given option is off i.e do no cut generation."
                  1  "Do a minimum of work in cut generation."
                  2  "Do a fair amount of work in cut generation."
                  3  "Be aggressive in cut generation."
                  4  "The given option is automatically determined by the optimizer." )
mipflowcovercuts.
     (            0  "The given option is off i.e do no cut generation."
                  1  "Do a minimum of work in cut generation."
                  2  "Do a fair amount of work in cut generation."
                  3  "Be aggressive in cut generation."
                  4  "The given option is automatically determined by the optimizer." )
mipcliquecuts.
     (            0  "The given option is off i.e do no cut generation."
                  1  "Do a minimum of work in cut generation."
                  2  "Do a fair amount of work in cut generation."
                  3  "Be aggressive in cut generation."
                  4  "The given option is automatically determined by the optimizer." )
mipimpliedboundcuts.
     (            0  "The given option is off i.e do no cut generation."
                  1  "Do a minimum of work in cut generation."
                  2  "Do a fair amount of work in cut generation."
                  3  "Be aggressive in cut generation."
                  4  "The given option is automatically determined by the optimizer." )
mipzerohalfcuts.
     (            0  "The given option is off i.e do no cut generation."
                  1  "Do a minimum of work in cut generation."
                  2  "Do a fair amount of work in cut generation."
                  3  "Be aggressive in cut generation."
                  4  "The given option is automatically determined by the optimizer." )
mipnodeselect.
     (            0  "The given option is automatically determined by the optimizer."
                  1  "Choose best bound first."
                  2  "Choose lowest depth first."
                  3  "Choose by pseudo costs."
                  4  "Choose from a hybrid estimate."
                  5  "Choose from a adaptive node search aim to balance the tree exploration." )
mipbranchselect.
     (            0  "The given option is automatically determined by the optimizer."
                  1  "Choose based on minimum infeasibility."
                  2  "Choose based on maximum infeasibility."
                  3  "Choose based on strong branching."
                  4  "Choose based on pseudo costs."
                  5  "Choose based on a hybrid pseudo price selection scheme." )
mipnodechildselect.
     (            0  "The given option is automatically determined by the optimizer."
                  1  "Choose up branch."
                  2  "Choose lo branch."
                  3  "Choose branch guided by incumbent." )
mipsolveaslp.
     (            0  "The given option is off."
                  1  "The given option is on." )
miplocalnodeheuristic.
     (            0  "Do not apply heuristic method."
                  1  "The optimizer automatically decides if the heuristic method should be applied."
                  2  "Heuristic will be used in a lightweight version."
                  3  "Heuristic will be used as much as deemed possible." )
miplocalnodeheuristicpump.
     (            0  "Do not apply heuristic method."
                  1  "The optimizer automatically decides if the heuristic method should be applied."
                  2  "Heuristic will be used in a lightweight version."
                  3  "Heuristic will be used as much as deemed possible." )
mipreducedcoststrengthroot.
     (            0  "The given option is off."
                  1  "The given option is on." )
mipreducedcoststrengthnode.
     (            0  "The given option is off."
                  1  "The given option is on." )
simusequadinf.
     (            0  "The given option is off."
                  1  "The given option is on." )
simsolveunscaled.
     (            0  "The given option is off."
                  1  "The given option is on." )
mipfeasfocus.
     (            0  "The given option is off."
                  1  "The given option is on." )
mipdotiming.
     (            0  "The given option is off."
                  1  "The given option is on." )
mippresolverootprobing.
     (            0  "The given option is off."
                  1  "The given option is on." )
miplocalnodeprobing.
     (            0  "The given option is off."
                  1  "The given option is on." )
miprootheuristicrens.
     (            0  "Do not apply heuristic method."
                  1  "The optimizer automatically decides if the heuristic method should be applied."
                  2  "Heuristic will be used in a lightweight version."
                  3  "Heuristic will be used as much as deemed possible." )
miplocalnodeheuristicrens.
     (            0  "Do not apply heuristic method."
                  1  "The optimizer automatically decides if the heuristic method should be applied."
                  2  "Heuristic will be used in a lightweight version."
                  3  "Heuristic will be used as much as deemed possible." )
miprootheuristicrins.
     (            0  "Do not apply heuristic method."
                  1  "The optimizer automatically decides if the heuristic method should be applied."
                  2  "Heuristic will be used in a lightweight version."
                  3  "Heuristic will be used as much as deemed possible." )
miplocalnodeheuristicrins.
     (            0  "Do not apply heuristic method."
                  1  "The optimizer automatically decides if the heuristic method should be applied."
                  2  "Heuristic will be used in a lightweight version."
                  3  "Heuristic will be used as much as deemed possible." )
miprootheuristicsins.
     (            0  "Do not apply heuristic method."
                  1  "The optimizer automatically decides if the heuristic method should be applied."
                  2  "Heuristic will be used in a lightweight version."
                  3  "Heuristic will be used as much as deemed possible." )
miplocalnodeheuristicsins.
     (            0  "Do not apply heuristic method."
                  1  "The optimizer automatically decides if the heuristic method should be applied."
                  2  "Heuristic will be used in a lightweight version."
                  3  "Heuristic will be used as much as deemed possible." )
miprootheuristicdiving.
     (            0  "Do not apply heuristic method."
                  1  "The optimizer automatically decides if the heuristic method should be applied."
                  2  "Heuristic will be used in a lightweight version."
                  3  "Heuristic will be used as much as deemed possible." )
miplocalnodeheuristicdiving.
     (            0  "Do not apply heuristic method."
                  1  "The optimizer automatically decides if the heuristic method should be applied."
                  2  "Heuristic will be used in a lightweight version."
                  3  "Heuristic will be used as much as deemed possible." )
mipadvancedpseudocost.
     (            0  "The given option is off."
                  1  "The given option is on." )
mipstronginit.
     (            0  "The given option is off."
                  1  "The given option is on." )
mipgomoryfractionalcuts.
     (            0  "The given option is off i.e do no cut generation."
                  1  "Do a minimum of work in cut generation."
                  2  "Do a fair amount of work in cut generation."
                  3  "Be aggressive in cut generation."
                  4  "The given option is automatically determined by the optimizer." )
mipknapsackcovercuts.
     (            0  "The given option is off i.e do no cut generation."
                  1  "Do a minimum of work in cut generation."
                  2  "Do a fair amount of work in cut generation."
                  3  "Be aggressive in cut generation."
                  4  "The given option is automatically determined by the optimizer." )
mipcutsuselexdual.
     (            0  "The given option is off."
                  1  "The given option is on." )
miprootheuristicswop.
     (            0  "Do not apply heuristic method."
                  1  "The optimizer automatically decides if the heuristic method should be applied."
                  2  "Heuristic will be used in a lightweight version."
                  3  "Heuristic will be used as much as deemed possible." )
mipnodeheuristicswop.
     (            0  "Do not apply heuristic method."
                  1  "The optimizer automatically decides if the heuristic method should be applied."
                  2  "Heuristic will be used in a lightweight version."
                  3  "Heuristic will be used as much as deemed possible." )
miprootheuristiceasy.
     (            0  "Do not apply heuristic method."
                  1  "The optimizer automatically decides if the heuristic method should be applied."
                  2  "Heuristic will be used in a lightweight version."
                  3  "Heuristic will be used as much as deemed possible." )
mipnodeheuristiceasy.
     (            0  "Do not apply heuristic method."
                  1  "The optimizer automatically decides if the heuristic method should be applied."
                  2  "Heuristic will be used in a lightweight version."
                  3  "Heuristic will be used as much as deemed possible." )
miptwomircuts.
     (            0  "The given option is off i.e do no cut generation."
                  1  "Do a minimum of work in cut generation."
                  2  "Do a fair amount of work in cut generation."
                  3  "Be aggressive in cut generation."
                  4  "The given option is automatically determined by the optimizer." )
mipusereformulation.
     (            0  "The given option is off."
                  1  "The given option is on." )
solwritenames.
     (            0  "The given option is off."
                  1  "The given option is on." )
cloudmodelmethod.
     (            2  "Will always create a new modelid, when a model is being uploaded or the optimizer is being called." )
cloudusehttpgzip.
     (            0  "The given option is off."
                  1  "The given option is on." )
cloudusehttps.
     (            0  "The given option is off."
                  1  "The given option is on." )
cloudagentselect.
     (            2  "Agents are selected by highest CPU mzh."
                  3  "Agents are selected by highest number of cores." )
mipdeterministic.
     (            0  "The given option is off."
                  1  "The given option is on." )
mipparallelrootsolve.
     (            0  "The given option is off."
                  1  "The given option is on." )
mipmaxvariability.
     (            0  "The given option is off."
                  1  "The given option is on." )
miphotstart.
     (            0  "The given option is off."
                  1  "The given option is on." )
miphotstartrins.
     (            0  "The given option is off."
                  1  "The given option is on." )
bndoptlevel.
     (            1  "Use moderate effort in bound optimizer module."
                  2  "Use aggressive effort in bound optimizer module."
                  3  "Use maximum effort in bound optimizer module." )
bndoptmip.
     (            0  "The given option is off."
                  1  "The given option is on." )
simusenetwork.
     (            0  "The given option is off."
                  1  "The given option is on." )
simusenetworkhotstart.
     (            0  "The given option is off."
                  1  "The given option is on." )
   /
$if exist optsulumlink.gms $include optsulumlink.gms
