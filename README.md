# SCENARIOS FOR STOCHASTIC OPTIMIZATION #

An important aspect of computational stochastic optimization is finding a set of scenarios that is representative but still enumerable. This library contains the most popular scenario generation methods as well as a test-bed to evaluate their performance on a simple stochastic decision problem for a number of probability distributions.

### Usage ###
The main application can be found in src/experiment/Generator.java which can also be compiled as a command line tool. The command line tool accepts a number of arguments which are summarized in the Javadoc of the main method.

The methods that are included in this library are described in this [working paper](http://nils.loehndorf.com/pdf/loehndorf_2015_scenario_reduction.pdf). The numerical section of the paper is based on experimental data that can be found in src/experiment/Experiment.java.

### Installation ###
1. Clone the repository and import it into an IDE, e.g., Eclipse.
2. Include all jars that are found under /jar into your build path.
3. Add /lib to the Java path, as it includes native libraries.

The version included in the repository contains the Sulum linear optimizer which was used to compute the Wasserstein distance between the scenarios and the original distribution. The repository only contains a version of the Sulum optimizer for Mac OS. For Linux or Windows, the corresponding native libs must be downloaded directly from [Sulum Optimization](http://sulumoptimization.com). Note that the optimizer is only neded to run the Experiment.java. The scenario generator will run without it.


### Contact ###
If you have any question contact me under nils@loehndorf.com.

### Copyright ###
The code is released under GPL v3.